import json

from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "status",
    ]
    encoders = {
        "status": StatusEncoder()
    }

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.all()
        return JsonResponse(
            {"presentations": presentations},
            encoder = PresentationListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        presentations = Presentation.create(**content)
        return JsonResponse(
            presentations,
            encoder= PresentationListEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            {"presentation": presentation},
            encoder = PresentationDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0},
        )
    else:
        content = json.loads(request.body)
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder = PresentationDetailEncoder,
            safe=False
        )
